import {Entity, PrimaryGeneratedColumn, Column, UsingJoinColumnIsNotAllowedError} from "typeorm";

@Entity()
export class Users {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    username: string;

    @Column()
    password: string;
    

}
