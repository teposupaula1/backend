import {NextFunction, Request, Response} from "express";
import { getRepository } from "typeorm";
import { Users } from "../entity/User";

export async function register(request: Request, response: Response, next: NextFunction){

    let userRepository = getRepository(Users);

    try {
        if (!request.body.username || !request.body.email || !request.body.password) {
            return response.status(400).send("Please provide all the necessary fields");
        } 
       const oldUser = userRepository.find({ where:
             { email: request.body.email }}) || null ;
        
         if ((await oldUser).length !=0) {
           return response.status(409).send("User Already Exist. Please Login");
        }
        return next();

     } catch (err) {
         console.log(err);  
     }
}