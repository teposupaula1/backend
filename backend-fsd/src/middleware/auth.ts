import {NextFunction, Request, Response} from "express";
var jwt = require('jsonwebtoken');

export function auth(request: Request, response: Response, next: NextFunction) {
    try {
        let token: String = request.headers.token;
        if (!jwt.verify(token, process.env.TOKEN_SECRET)) {
            return response.sendStatus(401).send("Invalid token");   
        }
    } catch (error) {
        console.log("Authorization Header error !! (not present or wrong format)");
        return response.sendStatus(401).send("Invalid token"); 
    }
    return next();
}
