const {Datastore}=require ('@google-cloud/datastore')

const datastore = new Datastore({
  projectId: 'vast-box-334914',
  keyFilename: 'datastore-credential.json'
});

exports.add= async(req,res)=>{

  res.set('Access-Control-Allow-Origin', "*")

  res.set('Access-Control-Allow-Methods', 'GET, POST');

  if (req.method === "OPTIONS") {
    // stop preflight requests here
    res.status(204).send('');
    return;
  }

  try {
    const key = datastore.key('user-log');
    const schedule = {
        key: key,
        data: [
            {
                name: 'start_date',
                value: req.query.start_date 
            },
            {
                name: 'end_date',
                value: req.query.end_date,
            },

        ],
    };

    await datastore.save(schedule);
    res.status(200).send("Succes");
}
catch (err) {
    res.status(500).send(err);
}
}


exports.get= async(req,res)=>{
    
  res.set('Access-Control-Allow-Origin', "*")
  try {
    const query = datastore.createQuery('user-log');
    const [schedules] = await datastore.runQuery(query);

    res.status(200).send(schedules);
} catch (err) {
    res.status(500).send(err);
}
}