import { AuthentificationController } from "./controller/AuthentificationController";
import {UserController} from "./controller/UserController";
import { auth } from "./middleware/auth";
import { register } from "./middleware/register";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    middlewares: [auth]
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middlewares: [auth]
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middlewares: [auth]
}, {
    method: "put",
    route:"/users/update/:id",
    controller: UserController,
    action: "update",
    middlewares: [auth]
},{
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middlewares: [auth]
}, {
    method: "post",
    route:"/users/login",
    controller: AuthentificationController,
    action: "login",
   
},{
    method: "post",
    route:"/users/register",
    controller: AuthentificationController,
    action: "register",
    middlewares:[register]
}
];