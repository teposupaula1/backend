import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response,NextFunction} from "express";
import {Routes} from "./routes";
import {Users} from "./entity/User";
import * as dotenv from 'dotenv';
import *as cors from "cors";

dotenv.config();

createConnection().then(async connection => {

    // create express app
    const allowedOrigins = ["http://localhost:3000"];
    const options : cors.CorsOptions = {
        origin : allowedOrigins
    }

    const app = express();
    app.use(cors(options));
    app.use(bodyParser.json());


    var router = express.Router();

    router.use("/", (request: Request, response: Response, next: NextFunction) => {
        console.log(`[${request.method}]: ${request.originalUrl}`);
        next();
    });

    // register express routes from defined application routes
    Routes.forEach(route => {
        if (route.middlewares) {

            route.middlewares.forEach((middleware) => {
                (router as any)[route.method](route.route, middleware);
            });
        }

        (router as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3001);

    app.use("/", router);


    

    console.log("Express server has started on port 3001. Open http://localhost:3001/users to see results");

}).catch(error => console.log(error));

