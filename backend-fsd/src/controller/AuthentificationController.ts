import { getRepository } from "typeorm";
import { Users } from "../entity/User";
import {NextFunction, Request, Response} from "express";
import * as jwt from "jsonwebtoken";


export class AuthentificationController {
    
    private userRepository = getRepository(Users);

    async login(request: Request, response: Response, next: NextFunction){

        let login = {
            email: request.body.email,
            password: request.body.password
        }


        let result =(await this.userRepository.findOne({where: login}));
        if(!result){
            response.status(401).send("Credentials mismatch!");
            return ;
        }

        var token = jwt.sign({result}, process.env.TOKEN_SECRET, { algorithm: process.env.TOKEN_ALGORITHM});
        response.status(200).send(token);

    }
    async register(request: Request, response: Response, next: NextFunction)
    {
        
         return this.userRepository.save(request.body); 
    }
   
}