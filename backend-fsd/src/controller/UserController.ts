import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { Users } from "../entity/User";

export class UserController {

    private userRepository = getRepository(Users);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id) || null;
        if (userToRemove != null) {
            await this.userRepository.remove(userToRemove);
            return "SUCCESS";
        }
        return "ERROR";
    }
    async update(request: Request, response: Response, next: NextFunction) {
        let updatedUser = await this.userRepository.findOne(request.params.id);
        updatedUser.email = request.body.email;
        updatedUser.password = request.body.password;
        updatedUser.username = request.body.username;
        return await this.userRepository.save(updatedUser);
    }

}